Perjatai 13. päivä, mutta ei tämä muutenkaan ole edistynyt, joten en usko
siihen.

14:00 kokeilen realmd:tä. Centrify ei ilmeisesti tue uusinta Ubuntua.

Googlen mukaan ( http://funwithlinux.net/2014/04/join-ubuntu-14-04-to-active-directory-domain-using-realmd/ ) realmd:kään ei toimi suoraan ja
bugi-raportteja mainitaan ilman linkkejä, josta ei ole kovin paljon iloa.

Yritän aluksi ilman ohjeita, kuten sen pitäisi toimia.

- `sudo su -`
- `apt-get install realmd`
- `realm --verbose join testidomain.local -U Adminstrator

Tässä vaiheessa on hyvä muistaa, ettei DNS-palvelimen asetuksia muistettu
vaihtaaa tällä virtuaalikoneella. Palvelimemme IP on siis `10.10.0.1`,
joka asetetaan DNS-palvelimeksi ja koska alin komento ei toiminut,
käynnistämme koneen uudelleen, jos se vaikkapa haluaisi ottaa
DNS-asetukset käyttöön ilman ylimääräistä säätämistä.

Nyt voidaan yrittää uudelleen ensimmäisellä ja viimeisellä komennolla,
joka ei toimi joten seuraan suosiolla näitä ohjeita. http://funwithlinux.net/2014/04/join-ubuntu-14-04-to-active-directory-domain-using-realmd/

Mutta, meillä ei juuri nyt ole DNS-palvelinta muuhun verkkoon, joten sekin
pitänee korjata. Lisätään DNS-palvelinlistan loppuun `, 8.8.8.8, 8.8.4.4`.

Ubuntun DNS-asetukset eivät toimi minun toiveideni mukaan, joten uudelleen
käynnistys ja asian korjaus.

```
sudo su -
apt install resolvconf
```

Muokataan tiedostoa `/etc/NetworkManager/NetworkManager.conf` ja
lisätään `#` sen rivin alkuun, jossa lukee `dns=dnsmasq`.

Muokataan tiedostoa `/etc/resolvconf/resolv.conf.d/head` ja lisätään sinne

```
# Windows Server
nameserver 10.0.0.1
```

Muokataan tiedostoa `/etc/resolvconf/resolf.conf.d/tail` ja lisätään sinne
rivi

```
# Google DNS, koska reititin ei toimi täällä DNS-palvelimena.
nameserver 8.8.8.8
nameserver 8.8.4.4
```

ja suoritetaan `resolvconf -u`.

Nyt voidaan jatkaa ohjeita, koska DNS vaikuttaa toimivan.

Pakettilistan päivittämisestä on aina hyvä aloittaa, kun asennetaan jotakin.

```
apt update
apt install realmd sssd
```

ja nyt asennus menee eteenpäinkin.

tmux näyttää ajaksi 2014-06-13 16:13+0300 ja joudun nyt menemään ulos
koiran kanssa, koska muut lähtivät sairaalaan.
