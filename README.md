<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Copy of GitHub gist files related to my work training in 2014, some of which looks worth looking at later.](#copy-of-github-gist-files-related-to-my-work-training-in-2014-some-of-which-looks-worth-looking-at-later)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Copy of GitHub gist files related to my work training in 2014, some of which looks worth looking at later.

Updated in early 2024 (10 years later) to add `.gitattributes` and
`.pre-commit-config.yaml` (and execute it) due to a suspicion that I might
wish to check something here in foreseeable future.

Content may be, or likely is, outdated, but maybe there is something to
salvage.
