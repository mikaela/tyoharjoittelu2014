<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Ubuntu Palvelimen asennus](#ubuntu-palvelimen-asennus)
- [Ubuntu Server](#ubuntu-server)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

11:45 aloitan asentamalla kloonattavalle koneelle Adobe Flashin ja Icedtean
(Java). Niistä ei ollut aiemmin puhetta, mutta ne tulevat vastaan ennemmin tai
myöhemmin, ellei niitä ole.

```
sudo su -
yum check-update
yum update
yum install icedtea-web
```

Adoben asennuspaketti taas ladataan osoitteesta http://get.adobe.com/flashplayer .
Valitaan `YUM for Linux`.

```
yum install ~user/Lataukset/adobe-release-x86_64-1.0-1.noarch.rpm
yum check-update
yum install flash-plugin
```

Nyt vaihdan salasanan tekstiviestillä ilmoitetuksi, joka ei kyllä minusta
vaikuta yhtään sen paremmalta, kuin Qwerty123, mutta seuraan ohjeita.

```
passwd root
passwd user
```

Fedora on muuten kanssani samaa mieltä.

> HUONO SALASANA: Salasana ei läpäise sanakirjatarkistusta - pohjautuu sanakirjan sanaan

Kaikki mitä tulee mieleen kloonattavalla koneella on nyt tehty ja siirryn
luokkaan 208 asentamaan DRBL:n. Yritin jo kotona katsoa vanhasta Moodlesta
miten se tapahtui Ubuntun kanssa, mutta se olikin jo ehtinyt lopettaa toimintansa.
DRBL:n sivuilla on tosin Fedora-ohjeet (tarkemmin sanoen RHEL) ja ilmeisesti
interfacen kloonaus ei tapahdu aivan samalla tavalla, kuin Ubuntulla.

Luokassa 208 ensimmäinen kone ei suostu käynnistymään tikulta, mutta sillä on
Ubuntu ja kirjoitan sillä tätä. Seuraava kone asentaa nyt Fedoraa.

Fedora on asentunut ja koska en välttämättä tarvitse graafista käyttöliittymää,
hoidan kaiken tällä Ubuntulla ja SSH:lla. Fedoran IP on toistaiseksi 10.10.100.13.

Asennan nyt DRBL:n ohjeiden mukaisesti http://drbl.sourceforge.net/installation/01-prepare-server.php .

http://drbl.sourceforge.net/installation/ip_alias.txt korvaan eth0:n em1:lla,
koska `ifconfig` sanoo niin. Asetan myös sille kiinteän IP:n.

```
DEVICE=em1
BOOTPROTO=static
BROADCAST=255.255.0.0
IPADDR=10.10.11.20
NETMASK=255.255.0.0
NETWORK=10.10.11.0
ONBOOT=yes
```

Fedora ei suostu toimimaan kanssani, joten asennan Ubuntu Serverin, jota käytettiin viimeksikin.

```
sudo su -
pv ~mikaela/Lataukset/ubuntu-14.04.1-server-amd64.iso|dd of=/dev/sdd
```

## Ubuntu Palvelimen asennus

- Suomi
- Asenna Ubuntu palvelinversio

- SUomi

- Atomaattinen näppäimistön tunnistus ja seurataan ohjeita. Se havaitsee
  todennäköisesti `se` joka kelpaa, koska suomen ja ruotsin näppäimistöt ovat
  identtisiä.

- drbl-ubuntu-server

- user
- user
- Qwerty123
- Qwerty123
- Ei

- Kyllä
- Kyllä

- Osioi itse

  - Poistetaan kaikki osiot /dev/sda:lta
  - Luodaan uusi / 80GB ja swap 2GB

- tyhjä (ei välityspalvelinta)

- Ei automaattisia päivityksiä

- [X] OpenSSH server

- Kyllä

## Ubuntu Server

Kirjaudutaan sisään `user` ja `Qwerty123`,

```
sudo su -
passwd
```

ja asetetaan kiinteä IP ja tehdään samalla alias drbl:lle.

Muokataan tiedostoa `/etc/network/interfaces`

```
auto lo
iface lo inet loopback

auto eth0
allow-hotplug eth0
iface eth0 inet static
address 10.10.11.20
netmask 255.255.0.0
gateway 10.10.0.1
dns-nameservers 8.8.8.8 8.8.4.4
iface eth0 inet6 auto

auto eth0:1
allow-hotplug eth0:1
iface eth0:1 inet static
address 192.168.100.254
netmask 255.255.255.0
iface eth0:1 inet6 auto
```

Uudelleenkäynnistys `reboot

Lisätään tiedostoon `/etc/apt/sources.list.d/drbl.list`

```
deb http://free.nchc.org.tw/drbl-core drbl stable
```

ja korvataan `/etc/apt/sources.list` paremmalla.

```
cd /etc/apt/
wget -O sources.list http://mikaela.info/shell-things/sources.list/ubuntu1404
gpg --keyserver keyserver.ubuntu.com --recv-keys 16126D3A3E5C1192 40009511D7E8DF3A
gpg --export 16126D3A3E5C1192 40009511D7E8DF3A|apt-key add -
apt-get update
apt-get install drbl
drblsrv -i
```

- n
- n
- n
- paketteja asennetaan
- 1

```
drblpush -i
```

- hekamil.lo
- penguinzilla
- drbl-ubuntu-server- \* Ilmeisesti isäntänimen olisi voinut määrittää täällä, mutta tämä ei
  mahdollisesti toimisi, kuten sen pitäisi, joten pysytään alkuperäisessä
  suunnitelmassa.
- eth0
- n - Do you want to collect them?
  - täällä ei pitäisi olla muita koneita, joiden kanssa menisi sekaisin.
- n - This is for the clients connected to DRBL server's ethernet network interface eth0:1
- 1a - What is the initial number do y
- 18 - How many DRBL clients
  - ellen laskenut aivan väärin.
- y - Accept
- 2 - Which mode do you prefer?
- 3 - Use Clonezilla live as the OS
- 1 - testing (Debian-based) \* Ubuntut eivät tunnu toimivan ja "epävakaa Debian on vakaampi, kuin
  vakaa Ubuntu"
- 2 - amd64
- /home/partimag - When using clonezilla, which directory in this server you want to store the saved image
- n - Do you want to set the pxelinux password
- y - Do you want to set the boot prompt for clients?
- 70 - How many 1/10 sec is the boot prompt timeout for clients?
- n - Do you want to use graphic background for PXE menu when client boots?
- y - NAT server
- n - Do you want to keep the old setting of existing DRBL clients if they exist?
- y - Do you want to continue?

```
dcs
```

- all
- clonezilla start
- beginner
- save-disk
- now
- <päivämäärä>.img
  - oletus
- sda
- skip
- no, skip
- -p poweroff
- 1000000
  - _2024-03-03: Tämän luvun perästä poistettiin pitkä lista tähtiä ja
    kenoviivoja, joita `prettier` halusi kovasti lisäillä jokainen
    suorituskerta._

Tulos (seuraavalla kerralla voit suorittaa tämän komennon):

```
drbl-ocs -b -q2 -j2 -sc -p poweroff -z1p -i 1000000 -l en_US.UTF-8 startdisk save 2014-07-30-15-img sda
```

Palaan nyt luokkaan 215, otan varmuuden vuoksi SSH:n käyttöön `systemctl enable sshd` ja `systemctl start sshd` ja käynnistän verkosta kloonauksen.

Tulos: toinen verkkokortti on oikea, mutta sanoo `NFS over TCP not available from 192.168.100.254`.
