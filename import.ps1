# Import list of Users From C:\dat.csv into $Userlist

$UserList=IMPORT-CSV C:\julkinen\datanomit.csv

# Step through Each Item in the List

FOREACH ($Person in $UserList) {

# Build Username Firstname.Lastname

#$Username=$Person.Username
$Username=$Person.Firstname+"."+$Person.Lastname

# Build Password from Firstname and Lastname

#$Password=$Person.Firstname+$Person.Lastname
$Password=$Person.Password

# Build the Displayname

$Name=$Person.Firstname+” “+$Person.Lastname

# Build and define Domain name

$Domain="@ekamih.lo"

# Build User Principal Name

$UPN=$Username+$Domain

# Build and define Home Directory path
# !!
$HDrive="C:\%username%"

# Build and define which Organizational Unit to create User inside

$Group=$Person.Group

$OU="OU=$Group,DC=ekamih,DC=lo"

# Create Account in Active Directory (AND HERE...WE...GO!)

New-ADUser -Name $Name –GivenName $Person.Firstname –Surname $Person.Lastname –DisplayName $Name –SamAccountName $Username -HomeDrive "H:" -HomeDirectory $HDrive –UserPrincipalName $UPN -Path $OU

# Set Password
#echo '$Password'
Set-ADAccountPassword -PassThru -Reset -Identity $Username -NewPassword (ConvertTo-SecureString -AsPlainText "$Password" -Force)

# Add User to Security Groups

Add-ADPrincipalGroupMembership -Identity $Username -MemberOf "vboxusers"

# Enable Account

Enable-ADAccount -Identity $Username

}
