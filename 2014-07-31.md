11:46 minulla on luokan 208 kone1 käynnissä. Yritän vielä DRBL:ää ja muita
vaihtoehtoja, koska ruuvimeisseli ja levyjen siirtäminen ja kloonaaminen
käsin kuulostaa viimeiseltä mahdolliselta vaihtoehdolta, mikäli mikään muu
ei toimi.

Sen kirjoittamisessa tikulle on kaksi tapaa, joita kokeilen ja kolmas vaatii
Windowsin hätätapauksessa.

```
sudo su -
pv ~mikaela/Lataukset/drbl-live-xfce-2.2.3-8-amd64.iso|dd of=/dev/sdd
```

Toimii ja valitaan DRBL live (ylin) ja sen jälkeen

- en_US.UTF-8 English
- Select keymap from full list
- pc / qwerty / Finnish / Standard / Standard
- 0 - continue to start X

- CloneZilla server

- n - lease ip
- local dev
- /

- All
- Beginner
- Save-disk
- Now in server
- <oletus>.img
- sda
- skip
- no skip
- poweroff
- 2000

Kloonaus onnistui! Sitten palautus. Käynnistän DRBL liven varmuuden vuoksi uudelleen.

- en_US.UTF-8
- Select keymap from full
- pc / qwerty/ Finnish / standard / stadard
- 0 - conitnue to start x

- CloneZilla Server
- n- lease
- local dev
- /

- All
- Beginner
- restore-disk
- -p poweroff
- äsken tehty levykuva
- sda
- broadcast
- clients to wait
- 17
  - koska koneita oli 18 - 1 joka on valmis.

Ja nyt käynnistellään 17 konetta verkosta. Joillakin tosin täytyi ottaa LAN-boot käyttöön.
