<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [15:48 Windows Server 2012 -virtuaalikoneen luonti.](#1548-windows-server-2012--virtuaalikoneen-luonti)
  - [Create Virtual Machine:](#create-virtual-machine)
    - [Hard drive](#hard-drive)
  - [Virtuaalikoneen muut asetukset](#virtuaalikoneen-muut-asetukset)
  - [Windows Server 2012 Asennus](#windows-server-2012-asennus)
  - [Määritys](#m%C3%A4%C3%A4ritys)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

15:45: aloitus viivästyi, mutta virtualboxin asennus aloitettu.

- Paketit: virtualbox virtualbox-dkms dkms

## 15:48 Windows Server 2012 -virtuaalikoneen luonti.

### Create Virtual Machine:

- Nimi: Windows Server 2012
- Type: Microsoft Windows
- Version: Windows 2012 (64-bit)

Annettu muistin olla oletus 2048MB, ei kai yksi muutaman käyttäjän AD, voi
niin paljon vaatia. Sitä voidaan tarvittaessa nostaa.

#### Hard drive

- Create a...
- VDI
- Dynamic
- Sijainti; Windows Server 2012\_, 25GB

### Virtuaalikoneen muut asetukset

Oikea klikkaus --> Asetukset

- System
  - Processor(s)
  - 4
- Display
  - Video
    - Ota 3D-kiihdytys käyttöön
    - Enable 2D Video Acceleration
  - Video Capture
    - Enable Video Capture
- Verkko
  - Sovitin 1
    Bridged Adapter

### Windows Server 2012 Asennus

Käynnistetään kone ja valitaan levykuva.

- Language to instll: English (United States)
- Time and currency format: Finnish (Finland)
- Keyboard or input method: Finnish

- Install

- Install now

- Windows Server 2012 R2 Standard (Server with a GUI)

- [x] I agree the license terms
- Next

- Custom
- Drive 0: Unallocated Space
- Next

- Adminstrator
- ADTesti123

### Määritys

- Networks: Yes

Verkko-asetukset

- Computer name: TESTIADSERVER
- IP: 10.0.1.1
- Subnet mask: 255.0.0.0
- Default gateway: —
- DNS1: 10.0.1.1

Uudelleenkäynnistys nimen vaihtamisen vuoksi.

Add roles and features

- Acdive Directory Domain Services
- DNS Server

- User Interfaces and Infastructure
  - Desktop Experience

Uudelleenkäynnistys taas.

Server Manager --> Post-deployment configuration (lippu)

Promote this server to a domain controller.

- Add a new forest
  - testidomain.local
- Windows Server 2012 R2
- DSRM passowrd: Qwerty123
- Next muutaman kerran.
- Install

Server manager, tools, Active Directory Users and Computers

testidomain.local

\*Add Organizational unit: testi

_Create a new user:
_ Mikaela Suomalainen: properties
_ Member of:
_ Adminstrators
\_ Domain Admins \* Salasanat: Qwerty123

Seuraavaksi vuorossa Ubuntun ja Fedoran asennukset.
